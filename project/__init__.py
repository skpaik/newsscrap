"""
============================================================================
Copyright (C) 2018 Xy Company Ltd - All Rights Reserved.
----------------------------------------------------------------------------
Created by: Sudipta K Paik on [20-Sep-2018 at 10:15 PM].
Email: sudipta@xycompany.com
----------------------------------------------------------------------------
Project: NewsScrap.
Code Responsibility: Application creation
============================================================================
"""

from flask import Flask

from .controllers.home_controller import home
from .controllers.news_controller import news

# Starting point of this application
# Add as many component of this application
app = Flask(__name__)
app.register_blueprint(home)
app.register_blueprint(news)
