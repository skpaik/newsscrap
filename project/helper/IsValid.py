"""
============================================================================
Copyright (C) 2018 Xy Company Ltd - All Rights Reserved.
----------------------------------------------------------------------------
Created by: Sudipta K Paik on [20-Sep-2018 at 10:15 PM].
Email: sudipta@xycompany.com
----------------------------------------------------------------------------
Project: NewsScrap.
Code Responsibility: All type of validation checking
============================================================================
"""
import validators


# Check a link is valid url or not
@staticmethod
def url(link):
    try:
        return validators.url(link)
    except:
        return False
