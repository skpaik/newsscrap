"""
============================================================================
Copyright (C) 2018 Xy Company Ltd - All Rights Reserved.
----------------------------------------------------------------------------
Created by: Sudipta K Paik on [20-Sep-2018 at 10:15 PM].
Email: sudipta@xycompany.com
----------------------------------------------------------------------------
Project: NewsScrap.
Code Responsibility: All News and related request controller
============================================================================
"""

import random
from bson.objectid import ObjectId

from flask import Blueprint

from project.services import news_service

news = Blueprint('news', __name__)


# Return all data in collection
@news.route('/api/news', methods=['GET'])
@news.route('/api/news/', methods=['GET'])
def get_all_news():
    return news_service.get_all_crawl()


# Search full text data based on keyword
@news.route("/api/news/<keyword>", methods=['GET'])
def search_news(keyword):
    if keyword:
        return news_service.find_by_full_text(keyword)
    else:
        return 'invalid paramters', 400


# Insert a single data in collection
@news.route("/api/news/insert", methods=['GET'])
def insert_demo_news():
    random_str = str(random.randint(1, 101))
    params = {
        "url": "news/url/" + random_str,
        "title": "title " + random_str,
        "tags": ["Sydney" + random_str, "Singapore" + random_str]
    }
    if params:
        return news_service.insert_one(params)
    else:
        return 'invalid paramters', 400


# Delete data from collection based on query
@news.route("/api/news/delete", methods=['GET'])
@news.route("/api/news/delete/", methods=['GET'])
@news.route("/api/news/delete/<_id>", methods=['GET'])
def delete(_id=None):
    try:
        if _id is None:
            return 'Delete Nothing', 200
        elif _id == "all":
            return news_service.delete_all()
        else:
            return news_service.delete_one({"_id": ObjectId(_id)})

    except Exception as error:
        return error, 404
