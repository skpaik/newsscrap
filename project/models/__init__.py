"""
============================================================================
Copyright (C) 2018 Xy Company Ltd - All Rights Reserved.
----------------------------------------------------------------------------
Created by: Sudipta K Paik on [20-Sep-2018 at 10:15 PM].
Email: sudipta@xycompany.com
----------------------------------------------------------------------------
Project: NewsScrap.
Code Responsibility: MongoDb initiation and model exposing
============================================================================
"""

from flask import Flask
from flask_pymongo import PyMongo

mongo = Flask(__name__)

MONGO_URI = "mongodb://localhost:27017/ScrapNewsDb"

mongo.config["MONGO_URI"] = MONGO_URI

instance_mongo = PyMongo(mongo)

# Collections
news_crawl_model = instance_mongo.db.CrawlNews

# Create Index for full text search
news_crawl_model.create_index([('title', 'text')])
