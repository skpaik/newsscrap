from project import app
from unittest import TestCase
import requests


class TestIntegrations(TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_thing(self):
        rv = self.app.get(':5000/api/news')
        assert b'world-europe' in rv.data

    def test_hello_world(self):
        response = requests.get('http://localhost:5000/api/news')
        assert b'world-europe' in response.json()
