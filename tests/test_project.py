import os
import tempfile

import pytest

from project import app


@pytest.fixture
def client():
    db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    client = app.test_client()

    # with start_app_.app_context():
    #     start_app_.init_db()

    yield client

    os.close(db_fd)
    os.unlink(app.config['DATABASE'])


def test_empty_db(client):
    """Start with a blank database."""

    rv = client.get('/')
    assert b'No entries here so far' in rv.data


def api_news_get(client):
    return client.get('/api/news')


def api_news_get_json(client):
    rv = client.get('/api/news')
    json_data = rv.get_json()
    assert b'world-europe' in json_data


def api_news_get_json(client):
    assert client.get('http://localhost:5000/api/news/').status_code == 200
