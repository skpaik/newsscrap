Challenge - News Content Collect and Store  
===================  
Using following technology  
  
1. Scrapy (scraping and crawling framework)  
1. Flask (micro web development framework based on Werkzeug)  
1. Mongodb (NoSql database)  
  
The project is split up into two subprojects located in the respective folders.  
We firstly scrape the www.bbc.com with the aim to get information for news (title, url, tags) we are interesting in. This information is persistently stored in the mongodb database.  
 The second subproject corresponds to a web application being responsible for exposing api of the data we gathered from BBC.  
  
Project structure  
-----------  
  
![Screenshot](https://bitbucket.org/skpaik/newsscrap/raw/72a4495c0a02dcaafc7095f2db52e2345b1ec157/docs/Project_Structure.jpg)  
  
  
Components  
-------------  

## scrapy\scrapnews
**Location: NewsScrap\scrapnews**   
Goal of our scraping application is to fetch information related to news. For example: title, url, tags, etc. We specify a url that corresponds to a list assembled by bbc itself. E.g. http://www.bbc.com). Then the scrapy spider parses this html and for every valid links existing there it acquires information. This information is later being stored to `CrawlNews` collection of mongodb database (`ScrapNewsDb`) by the implemented pipeline.  
  

## flask\project
**Location: NewsScrap\project**    
A web application was implemented to present the aforementioned movie related information in a human friendly manner. This application is backed up by a server provided by the flask framework. Server listens for user requests and dispatces these requests to the UI as JSON format.  
  
Installation  
-------------  

**Scrapping News**  
For terminal command use `sudo` whether applicable.  
  
1. Make sure mongodb install and running on your local computer.  
2. Create virtual env  
    `pip install virtualenv` on terminal  
3. Simple clone the repository  
    `git clone https://skpaik@bitbucket.org/skpaik/newsscrap.git`  
4. Change directory by  
   `cd scrapnews/scrapnews`  
  This operation will take some  
5. To scrape the news  
    `scrapy crawl news`   
 
 Here we will collect news from BBC.  
  
   
**Run web application**  
After finishing scrapping back to root of the project by `cd ..` two times.  
  
1. Use following command for install dependency
	`pip install -r requirements.txt`  
 2. To run the application use the following command.
	`python application.py`  
  
Starting the flask server(web application)  
-------------------------  
Once spider and pipeline have completed, the server can be started and content can be served to the user via the web browser. In order to start the server simply follow the steps:  
  
1. Back to root of the project by `cd ..` two times.  
  
1. Use following command for install dependency   
     `pip install -r requirements.txt`  
  
1. From editor configuration, configure project as `app.py` is the main script. and hit run.  
  
1. Or Run the application use the following command.  
  `python application.py`  
  
Check web page  
--------------  
Open your preferred browser and type in the location bar:  
`http://localhost:5000`  
  
  
API are exposed  
	1. `/api/news/`  
	  Will show all stored news (`as json format`) collected by scrapy
	2 . `/api/news/photo` 
		Will apply a full text search on title of keyword `photo` and show result  
	3. `/api/news/delete/all`  
		Will delete all news stored in database  
    4. `/api/news/delete/<_id>`  
	  Will delete a news stored in database where _id equal `_id`  
	5. `/api/news/insert`  
	  By hitting this api you can create a random entry in database     
            
**Hosting  in AWS**  
1. I try to host the codebase to aws on following url.  
2. URL: http://newsscrap.ap-southeast-1.elasticbeanstalk.com  
3. But after deploying it's not working. Though in my office I deploy several nodejs application on multi container docker.  
4. I reverted the code to sample application with small change.  
  
  
**Here I use:**  
1. AWS elasticbeanstalk  
2. Region: Singapore   
3. VPC: Default (Should Separate VPC)  
4. Subnet: Default (Should Public Subnet)  
5. Mongo Database Instance Location: Same Machine with Default Subnet(it Should be private subnet and only access via public subnet. Not from internet.)  
5. Access via Elastic IP to setup mongodb  
6. Security Group: Auto generated