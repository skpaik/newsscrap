import scrapy
import json
import validators


class NewsSpider(scrapy.Spider):
    name = "news"

    def start_requests(self):
        urls = [
            'http://www.bbc.com/'
        ]
        # urls = ['http://quotes.toscrape.com/page/3/', 'http://quotes.toscrape.com/page/4/',
        #         'http://quotes.toscrape.com/page/5/', ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, headers={
                "User-Agent": "Mozilla/5.0 (X11; Linux; rv:47.0) Gecko/20100101 Firefox/47.0"
            })

    def parse(self, response):
        page = response.url.split("/")[-2]
        # d = json.loads(response.body.decode())

        self.log("json.loads(response.body.decode())")

        for media_list__item in response.css('li.media-list__item'):

            if media_list__item is not None:
                block_link__overlay_link_url = media_list__item.css(
                    'a.block-link__overlay-link::attr(href)').extract_first()

                block_link__overlay_link_text = media_list__item.css(
                    'a.block-link__overlay-link::text').extract_first()

                block_link__overlay_link_tag = media_list__item.css(
                    'a.media__tag::text').extract()

                yield {
                    'url': block_link__overlay_link_url,
                    'title': block_link__overlay_link_text.strip(),
                    'tags': block_link__overlay_link_tag,
                }

                if block_link__overlay_link_url is not None and self.is_valid_url(
                        block_link__overlay_link_url):
                    yield response.follow(block_link__overlay_link_url, callback=self.parse)

        for a_bold_image_promo in response.css('a.bold-image-promo'):
            if a_bold_image_promo is not None:
                a_bold_image_promo_url = a_bold_image_promo.css(
                    '::attr(href)').extract_first()

                a_bold_image_promo_title = a_bold_image_promo.css(
                    'h3.bold-image-promo__title::text').extract_first()

                yield {
                    'url': a_bold_image_promo_url,
                    'title': a_bold_image_promo_title.strip()
                }

                if a_bold_image_promo_url is not None and self.is_valid_url(
                        a_bold_image_promo_url):
                    yield response.follow(a_bold_image_promo_url, callback=self.parse)

    def is_valid_url(self, link):
        try:
            return validators.url(link)
        except:
            return False
