"""
============================================================================
Copyright (C) 2018 Xy Company Ltd - All Rights Reserved.
----------------------------------------------------------------------------
Created by: Sudipta K Paik on [20-Sep-2018 at 10:15 PM].
Email: sudipta@xycompany.com
----------------------------------------------------------------------------
Project: NewsScrap.
Code Responsibility: All News and related Service provider, like
business logic implementation, data management
============================================================================
"""

from flask import jsonify
from bson import json_util
import json

from project.helper.logR import log
from project.models import news_crawl_model


# Return all data in collection
def get_all_crawl():
    try:
        res = json.loads(json_util.dumps(news_crawl_model.find()))
        return jsonify(res), 200
    except Exception as error:
        return error, 404


# Search full text data on title field
def find_by_full_text(keyword):
    log.info("keyword>>>>>>>>>>")
    log.info(keyword)
    try:
        result_keyword = news_crawl_model.find({"$text": {"$search": keyword}}).limit(10)
        res = json.loads(json_util.dumps(result_keyword))
        return jsonify(res), 200
    except Exception as error:
        return error, 404


# Insert single data in collection
def insert_one(params):
    try:
        news_crawl_model.insert(params)
        return 'Create Success', 200
    except Exception as error:
        return error, 404


# Data single data from collection
def delete_one(my_query):
    try:
        news_crawl_model.delete_one(my_query)
        return 'Delete one Success', 200
    except Exception as error:
        return error, 404


# Delete all data from collection
def delete_all():
    try:
        news_crawl_model.delete_many({})
        return 'Delete All Success', 200
    except Exception as error:
        return error, 404
