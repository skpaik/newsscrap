"""
============================================================================
Copyright (C) 2018 Xy Company Ltd - All Rights Reserved.
----------------------------------------------------------------------------
Created by: Sudipta K Paik on [20-Sep-2018 at 10:15 PM].
Email: sudipta@xycompany.com
----------------------------------------------------------------------------
Project: NewsScrap.
Code Responsibility: All home and related request controller
============================================================================
"""

from flask import Blueprint, render_template

home = Blueprint('home', __name__)


# Access the root of the web application
@home.route('/', methods=['GET'])
def home_page():
    return render_template('home.html')
