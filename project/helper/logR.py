"""
============================================================================
Copyright (C) 2018 Xy Company Ltd - All Rights Reserved.
----------------------------------------------------------------------------
Created by: Sudipta K Paik on [20-Sep-2018 at 10:15 PM].
Email: sudipta@xycompany.com
----------------------------------------------------------------------------
Project: NewsScrap.
Code Responsibility: Common place for loging
============================================================================
"""

import logging

logging.basicConfig(filename='logs/debug.log', level=logging.DEBUG)

log = logging.getLogger()
